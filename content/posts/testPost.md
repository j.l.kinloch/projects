---
title: "Fractals"
date: 2019-07-03T19:11:59+01:00
draft: false
tags: ["fractal","python","java"]
summary: "Fractals are a fascinating thing - the seemingly infinitly complex and beutifull 
patterns that emerge from basic maths is something that I always found fascinating.
Over time I have created a number of different fractals of different types in various languages, most of them small experiments.

##
![The Mandelbrot Fractal](/Fractal/test.JPG)"
---
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>

Fractals are a fascinating thing - the seemingly infinitly complex and beutifull 
patterns that emerge from basic maths is something that I always found fascinating.
Over time I have created a number of different fractals of different types in various languages, most of them small experiments.


---

## Mandelbrot

My first encounter with fractals was the Mandelbrot set. It is an amazing fractal and it captivated my imagination
and I often spent ages experimenting with fractal generators zooming in and looking at the patterns.
Eventually I decided I wanted to figure out how they worked, and it turned out to be simpler than I had thought.

So I then decided to make a mandelbrot renderer myself.

At the time Java was the main language I used and the one in which I knew how to
draw to screen the most.

Drawing the mandelbrot fractal turned out to be quite simple - effectivly it was just
 a case of iterating
 the formula \\(z=z^2 + c\\) where \\(z\\) starts at zero and \\(c\\) is the position of the pixel as a complex numer.
Then the pixel was coloured based how long it takes for \\(z\\) to start increasing rapidly.

Unfortunatly I am unsure what/where the first Mandelbrot 
renderer I made was - it has quite likely been lost as it was before I had used source controll 
and at the time didnt think the program was worth backing up - it was just a random experiement. I was able to find several others that I have made

However I didnt forget how to make it and since then have recreated a few minor varients - one 
even rendered jula fractals as well. One of them was similar to a mandelbrot but rendered using different iterations over time in an animation.

##### A Julia Fractal
<img src="/Fractal/Julia1.JPG" alt="drawing" height="300"/>
##### A Mandelbrot Fractal
<img src="/Fractal/FractalMandel.JPG" alt="drawing" height="300"/>
##### A Mandelbrot Variation
<img src="/Fractal/Iterations.JPG" alt="drawing" height="300"/>
##### A Different Julia Fractal
<img src="/Fractal/Julia2.JPG" alt="drawing" height="300"/>


## Recursive Fractals

Other means of fractals exist, and these can also look interesting. 
I created a number of different fractals such as the Dragon Curve, the Koch Snowflake and some varients/experiemens of those fractals.
These were all made in python - it was well suited to quick experimenting in my spare time between classes and had a convinient turtle library for drawing line based fractals.
They all use some kind of recursive algorithm to produce the pattern down to a specified depth.
##### A Koch Curve
<img src="/Fractal/kochEdge.JPG" alt="drawing" height="300"/>
##### A Dragon Curve
<img src="/Fractal/DragonCurve.JPG" alt="drawing" height="300"/>
##### A Custom Varient of the Koch Snowflake
<img src="/Fractal/PythonTurtleSuperSnow.JPG" alt="drawing" height="300"/>


